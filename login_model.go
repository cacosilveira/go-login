package main

import (
	"context"
	_ "context"
)

func (login *Login) getByUsername(ctx context.Context, username string) error {
	err := dbPool.QueryRow(ctx, "select id, username, password, status from users where username=$1 and status!=0 limit 1", username).
		Scan(&login.Id, &login.Username, &login.Password, &login.Status)
	return err
}

func (login *Login) getById(ctx context.Context, username string) error {
	err := dbPool.QueryRow(ctx, "select id, username, password, status from users where id=$1 and status!=0 limit 1", username).
		Scan(&login.Id, &login.Username, &login.Password, &login.Status)
	return err
}
