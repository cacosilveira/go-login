module cacosilveira/login

go 1.18

require (
	github.com/joho/godotenv v1.4.0
	golang.org/x/crypto v0.2.0
)

require (
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/puddle/v2 v2.1.2 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	golang.org/x/sync v0.0.0-20220923202941-7f9b1623fab7 // indirect
	golang.org/x/text v0.4.0 // indirect
)

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/gorilla/sessions v1.2.1
	github.com/jackc/pgx/v5 v5.1.0
)
