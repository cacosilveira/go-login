package main

import (
	"context"
	"log"
	"net/http"
	"testing"

	"github.com/joho/godotenv"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func TestGetByUsername(t *testing.T) {

	dbPool = getDBConnection(context.Background())
	defer dbPool.Close()

	tests := []struct {
		name     string
		method   interface{}
		wantErr  bool
		username string
	}{
		{
			name:     "user found",
			method:   http.MethodPost,
			wantErr:  false,
			username: "admin",
		},
		{
			name:     "user not found",
			method:   http.MethodPost,
			wantErr:  true,
			username: "walla09090",
		},
	}
	for _, tt := range tests {
		ctx := context.Background()
		ctx = context.WithValue(ctx, "method", tt.method)
		login := Login{}
		err := login.getByUsername(ctx, tt.username)
		t.Logf("%v", tt.name)
		t.Logf("%v", err)
		if (err != nil) != tt.wantErr {
			t.Errorf("Login.getByUsername() error: %v, wantErr: %v, username: %v", err, tt.wantErr, tt.username)
			return
		}

	}
}

func TestGetById(t *testing.T) {

	dbPool = getDBConnection(context.Background())
	defer dbPool.Close()

	tests := []struct {
		name    string
		method  interface{}
		wantErr bool
		id      string
	}{
		{
			name:    "user found",
			method:  http.MethodPost,
			wantErr: false,
			id:      "1",
		},
		{
			name:    "user not found",
			method:  http.MethodPost,
			wantErr: true,
			id:      "8374",
		},
	}
	for _, tt := range tests {
		ctx := context.Background()
		ctx = context.WithValue(ctx, "method", tt.method)
		login := Login{}
		err := login.getById(ctx, tt.id)
		t.Logf("%v", tt.id)
		t.Logf("%v", err)
		if (err != nil) != tt.wantErr {
			t.Errorf("Login.getByUsername() [%v] error: %v, wantErr: %v, username: %v", tt.name, err, tt.wantErr, tt.id)
			return
		}

	}
}
