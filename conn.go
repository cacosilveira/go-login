package main

import (
	"context"
	"log"
	"os"
	"time"

	"github.com/jackc/pgx/v5/pgxpool"
)

//The connection pool
var dbPool *pgxpool.Pool

//Return the connection or connect if needed
func getDBConnection(ctx context.Context) *pgxpool.Pool {
	log.Printf("Getting db connection")
	dbPool, err := pgxpool.New(ctx, os.Getenv("PG_CONNECTION"))
	if err != nil {
		log.Printf("got error: %v\n", err)
	}

	// Try connecting to the database a few times before giving up
	for i := 1; i < 3 && err != nil; i++ {
		// Sleep a bit before trying again
		time.Sleep(time.Duration(i*i) * time.Second)
		log.Printf("trying to connect to the db server (attempt %d)...\n", i)
		dbPool, err = pgxpool.New(ctx, os.Getenv("PG_CONNECTION"))
		if err != nil {
			log.Printf("got error: %v\n", err)
		}
	}

	// Stop execution if the database was not initialized
	if dbPool == nil {
		log.Fatalln("could not connect to the database")
	}

	// Get the postgres connection from the pool and check the database connection
	db, err := dbPool.Acquire(ctx)
	if err != nil {
		log.Fatalf("failed to get connection on startup: %v\n", err)
	}

	if err := db.Conn().Ping(ctx); err != nil {
		log.Fatalln(err)
	}

	// Add the connection back to the pool
	db.Release()

	return dbPool
}
