-- it's postgres

CREATE TABLE public.status (
	id int4 NOT NULL,
	"name" varchar NOT NULL,
	CONSTRAINT status_pk PRIMARY KEY (id)
);

CREATE TABLE public.users (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	username varchar NOT NULL,
	"password" varchar NOT NULL,
	status int4 NOT NULL DEFAULT 0,
	CONSTRAINT users_pk PRIMARY KEY (id)
);
ALTER TABLE public.users ADD CONSTRAINT users_fk FOREIGN KEY (status) REFERENCES public.status(id);
INSERT INTO users (username,"password",status) VALUES
	 ('admin','$2a$08$PscStVTctAr5ipNi9kyZ/.k5wItLGhZVDYVNqW6P71mpQd2imv5cO',1),
	 ('testland','$2a$08$PscStVTctAr5ipNi9kyZ/.k5wItLGhZVDYVNqW6P71mpQd2imv5cO',1);
