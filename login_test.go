package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/joho/godotenv"
)

var readData *http.Request
var writeData http.ResponseWriter

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func TestAmIOn(t *testing.T) {

	dbPool = getDBConnection(context.Background())
	defer dbPool.Close()

	req, err := http.NewRequest("GET", "/amion", nil)
	if err != nil {
		t.Fatal(err)
	}

	//Lets test the AmIOn

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AmIOnHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `0`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestLogin(t *testing.T) {
	dbPool = getDBConnection(context.Background())
	defer dbPool.Close()

	//testing the login
	//The last test must return true so we can test the logout
	tests := []struct {
		username   string
		password   string
		statusCode int
		session    bool
	}{
		{ //this must return true
			username:   "admin",
			password:   "admin",
			statusCode: http.StatusUnauthorized,
			session:    false,
		},
		{
			username:   "admin",
			password:   "password",
			statusCode: http.StatusOK,
			session:    true,
		},
	}

	for _, tt := range tests {
		form := url.Values{}
		form.Add("username", tt.username)
		form.Add("password", tt.password)

		req, err := http.NewRequest("POST", "/login", strings.NewReader(form.Encode()))
		if err != nil {
			t.Fatal(err)
		}
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(LoginHandler)

		// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
		// directly and pass in our Request and ResponseRecorder.
		handler.ServeHTTP(rr, req)

		// Check the status code is what we expect.
		if status := rr.Code; status != tt.statusCode {
			t.Errorf("Wrong status code: Got %v; Want %v.\nusername: %v\npassword: %v",
				status, tt.statusCode, tt.username, tt.password)
		}

		//Testing the session creation
		session, _ := store.Get(req, "session.id")
		//The session returns nil when not logged
		if tt.session == true {
			if session.Values["authenticated"] != tt.session {
				t.Errorf("User session error: Got %v; Want %v.\nusername: %v\npassword: %v",
					tt.session, session.Values["authenticated"], tt.username, tt.password)
			}
		} else {
			if session.Values["authenticated"] != nil {
				t.Errorf("User session error: Got %v; Want %v.\nusername: %v\npassword: %v",
					tt.session, session.Values["authenticated"], tt.username, tt.password)
			}
		}
	}

}

func TestWhoAmI(t *testing.T) {
	dbPool = getDBConnection(context.Background())
	defer dbPool.Close()

	username := "admin"
	password := "password"

	form := url.Values{}
	form.Add("username", username)
	form.Add("password", password)

	req, err := http.NewRequest("POST", "/login", strings.NewReader(form.Encode()))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(LoginHandler)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: Got %v; Want 200.\nusername: %v\npassword: %v",
			status, username, password)
	}

	//saving the cookies so we can test the logged user
	cookie := rr.Result().Cookies()

	req2, err := http.NewRequest("POST", "/whoami", strings.NewReader(form.Encode()))
	if err != nil {
		t.Fatal(err)
	}
	//inserting the cookies back
	for i := range cookie {
		req2.AddCookie(cookie[i])
	}
	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr2 := httptest.NewRecorder()
	handler2 := http.HandlerFunc(WhoAmIHandler)
	handler2.ServeHTTP(rr2, req2)

	if rr2.Code != 200 {
		t.Errorf("Error getting the user data. We the status code: %v, body: %v",
			rr2.Code, rr2.Body.String())
	}

	//let's put the json back on the Login struct
	login := Login{}
	json.Unmarshal([]byte(rr2.Body.String()), &login)

	if login.Username != username {
		t.Errorf("Wrong response from Who Am I: got %v want %v",
			login.Username, username)
	}
}
