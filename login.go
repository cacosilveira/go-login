package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/joho/godotenv"
	"golang.org/x/crypto/bcrypt"
)

var store = sessions.NewCookieStore([]byte(os.Getenv("SESSION_SECRET")))

type Login struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"-"`
	Status   int    `json:"status"`
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		http.Error(w, "400 Bad Request",
			http.StatusBadRequest)
		return
	}

	username := r.PostForm.Get("username")
	password := r.PostForm.Get("password")

	login := Login{}
	err = login.getByUsername(r.Context(), username)

	//user found
	if err == nil {
		session, _ := store.Get(r, "session.id")
		err := bcrypt.CompareHashAndPassword([]byte(login.Password), []byte(password))
		//check if the password matches
		if err == nil {
			//now we store our user and state
			log.Printf("user %v logged in\n", login.Username)
			session.Values["authenticated"] = true
			session.Values["user"] = login.Id
			session.Save(r, w)
		} else {
			http.Error(w, "401 Unauthorized",
				http.StatusUnauthorized)
			return
		}
	} else {
		//same message, we won't tell the user exists
		message := "401 Unauthorized"
		if os.Getenv("ENV") == "dev" {
			message = "User not found"
		}
		http.Error(w, message,
			http.StatusUnauthorized)
		return
	}
	w.Write([]byte("200 Ok"))
}

func terminateSession(session sessions.Session) {
	session.Options.MaxAge = -1
	//just to ba safe, unset the authenticated and user
	session.Values["authenticated"] = false
	session.Values["user"] = nil
}

//log the user out
func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "session.id")
	if (session.Values["authenticated"] != nil) && session.Values["authenticated"] != false {
		terminateSession(*session)
		session.Save(r, w)
	} else {
		http.Error(w, "401 Unauthorized",
			http.StatusUnauthorized)
		return
	}
	w.Write([]byte(""))
}

func DashboardHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "session.id")
	if (session.Values["authenticated"] != nil) && session.Values["authenticated"] != false {
		w.Write([]byte(time.Now().String()))
	} else {
		http.Error(w, "403 Forbidden", http.StatusForbidden)
	}
}

//return the user data
func WhoAmIHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "session.id")

	if (session.Values["authenticated"] != nil) && session.Values["authenticated"] != false {

		userid := fmt.Sprintf("%v", session.Values["user"])
		log.Printf("Searching for user #%v\n", userid)
		login := Login{}
		err := login.getById(r.Context(), userid)

		if err == nil {

			loginJson, _ := json.Marshal(login)
			w.Header().Set("Content-Type", "application/json")
			w.Write([]byte(loginJson))
			return

		} else {
			//The user was deleted or blocked, let's log them out
			terminateSession(*session)
			http.Error(w, "403 Forbidden", http.StatusForbidden)
			return
		}

	} else {

		http.Error(w, "403 Forbidden", http.StatusForbidden)
		return

	}
}

//just so we can tell if the user is logged from the other end
func AmIOnHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "session.id")
	log.Printf("user %v is on\n", session.Values["user"])
	if (session.Values["authenticated"] != nil) && session.Values["authenticated"] != false {
		w.Write([]byte("1"))
	} else {
		w.Write([]byte("0"))
	}
}

func main() {
	log.Printf("Initiating...")
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	dbPool = getDBConnection(context.Background())
	defer dbPool.Close()

	server := mux.NewRouter().StrictSlash(true)
	server.HandleFunc("/login", LoginHandler)
	server.HandleFunc("/dashboard", DashboardHandler)
	server.HandleFunc("/logout", LogoutHandler)
	server.HandleFunc("/whoami", WhoAmIHandler)
	server.HandleFunc("/amion", AmIOnHandler)
	http.Handle("/", server)

	srv := &http.Server{
		Handler: server,
		Addr:    os.Getenv("SERVER"),
		// enforcing timeouts
		WriteTimeout: 3 * time.Second,
		ReadTimeout:  3 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
